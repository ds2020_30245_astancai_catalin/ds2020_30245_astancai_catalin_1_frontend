import React from 'react'
import logo from './commons/images/icon.png';

import {
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    Navbar,
    NavbarBrand,
    NavLink,
    UncontrolledDropdown
} from 'reactstrap';

const textStyle = {
    color: 'white',
    textDecoration: 'none'
};

const NavigationBar = () => (
    <div>

        <Navbar color="white" light expand="md">
            <NavbarBrand href="/">
                <img src={logo} width={"50"}
                     height={"35"} />
            </NavbarBrand>

            <Nav  className="mr-auto" navbar>

                <NavLink
                href="/caregiver" ><strong>Caregiver</strong>
                </NavLink>



                <NavLink href="/patient"><strong>Patient</strong></NavLink>



                <NavLink href="/medication"><strong>Medication</strong></NavLink>

                <NavLink href="/"><strong>LOGOUT</strong></NavLink>


            </Nav>
        </Navbar>
    </div>
);

export default NavigationBar
