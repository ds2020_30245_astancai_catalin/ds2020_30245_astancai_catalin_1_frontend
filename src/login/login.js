import React from 'react';

import BackgroundImg from '../commons/images/future-medicine.jpg';

import {Button, Col, Container, FormGroup, Input, Jumbotron, Label, Row} from 'reactstrap';
import NavigationBar from "../navigation-bar";
import {Switch} from "react-router-dom";
import validate from "../person-data/person/validators/person-validators";
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import * as API_USERS from "../doctor-data/caregiver/api/caregiver-api";
import * as API_PATIENT from "../doctor-data/patient/api/patient-api";
import * as API_PERSON from "../person-data/person/api/person-api";
import Redirect from "react-router-dom/Redirect";

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: 'auto',
    height: "1920px",
    backgroundImage: `url(${BackgroundImg})`
};
const textStyle = {color: 'white', };

class Login extends React.Component {


    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);
        this.reloadHandler = this.props.reloadHandler;

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,
            listOfUsers:[],
            flagCareGiver:false,
            flagPatient:false,
            flagDoctor:false,
            formControls: {
                username: {
                    value: '',
                    placeholder: 'username',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                password: {
                    value: 'root',
                    placeholder: 'password',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    loginUser(user) {



         API_USERS.getCareGiver( (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                var caregiver;
                var patient
                this.state.listOfUsers= result;
                console.log("Successfully logged caregiver with id: " + user.username);
                this.state.flagCareGiver = false;
                for(let i=0 ; i< this.state.listOfUsers.length;i++){
                    if( this.state.listOfUsers[i].username === user.username && this.state.listOfUsers[i].password === user.password )
                        this.state.flagCareGiver = true;
                }

                this.reload();


            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });


        API_PATIENT.getPatient( (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                var caregiver;
                var patient
                this.state.listOfUsers= result;
                console.log("Successfully logged caregiver with id: " + user.username);
                this.state.flagPatient = false;
                for(let i=0 ; i< this.state.listOfUsers.length;i++){
                    if( this.state.listOfUsers[i].username === user.username && this.state.listOfUsers[i].password === user.password )
                        this.state.flagPatient = true;
                }

                this.reload();


            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });

        API_USERS.getDoctor( (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {

                this.state.listOfUsers= result;
                console.log("Successfully logged caregiver with id: " + user.username);
                this.state.flagDoctor = false;
                for(let i=0 ; i< this.state.listOfUsers.length;i++){
                    if( this.state.listOfUsers[i].username === user.username && this.state.listOfUsers[i].password === user.password )
                        this.state.flagDoctor = true;
                }

                this.reload();


            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });


    }

    handleSubmit() {
        let user = {
            username: this.state.formControls.username.value,
            password: this.state.formControls.password.value,
        };
        console.log(user);
        this.loginUser(user);
    }


    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
      //  this.fetchMedication();
    }
    render() {



        if(this.state.flagCareGiver ) {
            return (<Redirect to={{pathname: "/caregiverView",}}/>)
        }

        if(this.state.flagPatient ) {
            return (<Redirect to={{pathname: "/patientView",}}/>)
        }

        if(this.state.flagDoctor ) {
            return (<Redirect to={{pathname: "/doctor",}}/>)
        }


                return (

                    <div>
                        <Jumbotron fluid style={backgroundStyle}>
                            <FormGroup id='username'>
                                <Label for='usernameField' style={textStyle}> User: </Label>
                                <Input name='username' id='usernameField'
                                       placeholder={this.state.formControls.username.placeholder}
                                       onChange={this.handleChange}
                                       defaultValue={this.state.formControls.username.value}
                                       touched={this.state.formControls.username.touched ? 1 : 0}
                                       valid={this.state.formControls.username.valid}
                                       required
                                />
                                {this.state.formControls.username.touched && !this.state.formControls.username.valid &&
                                <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                            </FormGroup>


                            <FormGroup id='password'>
                                <Label for='passwordField' style={textStyle} > Password: </Label>
                                <Input type="password" name='password' id='passwordField'
                                       placeholder={this.state.formControls.password.placeholder}
                                       onChange={this.handleChange}
                                       defaultValue={this.state.formControls.password.value}
                                       touched={this.state.formControls.password.touched ? 1 : 0}
                                       valid={this.state.formControls.password.valid}
                                       required
                                />
                                {this.state.formControls.password.touched && !this.state.formControls.password.valid &&
                                <div className={"error-message row"}> * WRONG PASSWORD </div>}
                            </FormGroup>
                            <Row>
                                <Col sm={{size: '4', offset: 8}}>
                                    <Button  type={"submit"} disabled={!this.state.formIsValid}
                                            onClick={this.handleSubmit}> LOGIN </Button>

                                </Col>
                            </Row>

                            {
                                this.state.errorStatus > 0 &&
                                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                            }
                        </Jumbotron>
                    </div>
                )


    };
}

export default Login
