import {HOST} from '../../../commons/hosts';
import RestApiClient from "../../../commons/api/rest-client";


const endpoint = {
    get_caregiver: '/caregiver',
    get_doctor: '/doctor',
    post_caregiver: "/caregiver",
    update_caregiver:"/caregiver/update",
    delete_caregiver:"/caregiver/delete/"

};
function getDoctor(callback) {
    let request = new Request(HOST.backend_api + endpoint.get_doctor, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


function getCareGiver(callback) {
    let request = new Request(HOST.backend_api + endpoint.get_caregiver, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getCareGiverID(params, callback){
    let request = new Request(HOST.backend_api + endpoint.get_caregiver + params.id, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function insertCareGiver(user, callback){
    let request = new Request(HOST.backend_api + endpoint.post_caregiver , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function updateCareGiver(user, callback){
    let request = new Request(HOST.backend_api + endpoint.update_caregiver , {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });


    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deleteCaregiver(params, callback){
    let request = new Request(HOST.backend_api + endpoint.delete_caregiver + params, {
        method: 'DELETE'
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}






export {
    getCareGiver,
    getCareGiverID,
    insertCareGiver,
    updateCareGiver,
    deleteCaregiver,
    getDoctor
};
