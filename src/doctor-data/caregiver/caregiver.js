import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader, Nav, NavLink,
    Row
} from 'reactstrap';
import Table from "../../commons/tables/table"
import CareGiverForm from "./caregiver-form";
import * as API_USERS from "./api/caregiver-api"
import NavigationBar from "../../navigation-bar";
import CareGiverFormUpdate from "./caregiver-update";
import NavigationBarPatient from "../../navigation-bar-patient";
import NavigationBarCaregiver from "../../navigation-bar-caregiver";
import Redirect from "react-router-dom/Redirect";

var columns




const filters = [
    {
        accessor: 'cauta',
    }
];

class Caregiver extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.toggleFormUpdate = this.toggleFormUpdate.bind(this);
        this.toggleFormDelete = this.toggleFormDelete.bind(this);

        this.reload = this.reload.bind(this);
        this.reloadUpdate = this.reloadUpdate.bind(this);
        this.reloadDelete = this.reloadDelete.bind(this);


        this.state = {
            selected: false,
            selectedUpdate:false,
            selectedDelete:false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
        columns = [
            {
                Header: 'Username',
                accessor: 'username',
            },


            {
                Header: 'ID',
                accessor: 'id',
            },

            {
                Header: 'Name',
                accessor: 'name',
            },
            {
                Header: 'BirthDate',
                accessor: 'birthDate',
            },

            {
                Header: 'Gender',
                accessor: 'gender',
            },

            {
                Header: 'Address ',
                accessor: 'address',
            },

            {
                Header: 'Delete',
                Cell: ({original}) => (
                    <button value={original.id} onClick={this.deleteCareGiver.bind(this, original.id)}>DELETE</button>
                )
            },



        ];
    }



    deleteCareGiver(id) {
        console.log("salut "+id);
        return API_USERS.deleteCaregiver(id, (result, status, error) => {
            if (result !== null && status === 200) {

                console.log("Successfully deleted patient with id: " + result);
                this.reloadDelete();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }
    updateCareGiver(id) {
        console.log("salut "+id);

        return API_USERS.updateCareGiver(id, (result, status, error) => {
            if (result !== null && status === 200) {
                console.log("Successfully deleted patient with id: " + result);
                this.reloadUpdate();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }




    componentDidMount() {
        this.fetchCareGivers();
    }

    fetchCareGivers() {
        return API_USERS.getCareGiver((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }
    toggleFormUpdate(){
        this.setState({selectedUpdate: !this.state.selectedUpdate});
    }
    toggleFormDelete() {
        this.setState({selectedDelete: !this.state.selectedDelete});
    }


    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchCareGivers();
    }

    reloadUpdate(){
        this.setState({
            isLoaded: false
        });
        this.toggleFormUpdate();
        this.fetchCareGivers();

    }

    reloadDelete() {
        this.setState({
            isLoaded: false
        });
        this.toggleFormDelete();
        this.fetchCareGivers();
    }


    render() {
        return (
            <div>
                <NavigationBarCaregiver/>
                <NavLink href="/doctor"><strong>HOME</strong></NavLink>


                <CardHeader>
                    <strong> CAREGIVER </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '10', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm}>Add Caregiver </Button>
                            <p> </p>
                            <Button color="primary" onClick={this.toggleFormUpdate}>UPDATE Caregiver </Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '10', offset: 1}}>
                            {this.state.isLoaded &&
                            <Table
                                data={this.state.tableData}
                                columns={columns}
                                search={filters}
                                pageSize={3}
                            />
                            }

                            {this.state.errorStatus > 0 &&
                            <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />
                            }
                        </Col>
                    </Row>
                </Card>


                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add CareGivers: </ModalHeader>
                    <ModalBody>
                        <CareGiverForm reloadHandler={this.reload}/>

                    </ModalBody>

                </Modal>
                <Modal isOpen={this.state.selectedUpdate} toggle={this.toggleFormUpdate}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormUpdate}>UPDATE CAREGIVERS: </ModalHeader>
                    <ModalBody>
                        <CareGiverFormUpdate reloadHandler={this.reloadUpdate}/>
                    </ModalBody>

                </Modal>



            </div>
        )

    }
}


export default Caregiver;
