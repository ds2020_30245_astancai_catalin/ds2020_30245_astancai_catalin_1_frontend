import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import Table from "../../commons/tables/table"
import CareGiverForm from "./caregiver-form";
import * as API_USERS from "./api/caregiver-api"
import NavigationBar from "../../navigation-bar";
import CareGiverFormUpdate from "./caregiver-update";
import NavigationBarPatient from "../../navigation-bar-patient";
import NavigationBarCaregiver from "../../navigation-bar-caregiver";

var columns




const filters = [
    {
        accessor: 'cauta',
    }
];

class CaregiverView extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.toggleFormUpdate = this.toggleFormUpdate.bind(this);
        this.toggleFormDelete = this.toggleFormDelete.bind(this);

        this.reload = this.reload.bind(this);
        this.reloadUpdate = this.reloadUpdate.bind(this);
        this.reloadDelete = this.reloadDelete.bind(this);


        this.state = {
            selected: false,
            selectedUpdate:false,
            selectedDelete:false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };

    }








    componentDidMount() {
        this.fetchCareGivers();
    }

    fetchCareGivers() {
        return API_USERS.getCareGiver((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }
    toggleFormUpdate(){
        this.setState({selectedUpdate: !this.state.selectedUpdate});
    }
    toggleFormDelete() {
        this.setState({selectedDelete: !this.state.selectedDelete});
    }


    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchCareGivers();
    }

    reloadUpdate(){
        this.setState({
            isLoaded: false
        });
        this.toggleFormUpdate();
        this.fetchCareGivers();

    }

    reloadDelete() {
        this.setState({
            isLoaded: false
        });
        this.toggleFormDelete();
        this.fetchCareGivers();
    }


    render() {
        return (
            <div>
                <NavigationBarCaregiver/>
                <CardHeader>
                    <strong> CAREGIVER </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '10', offset: 1}}>

                            <Button color="primary" onClick={this.toggleFormUpdate}>UPDATE Caregiver </Button>
                        </Col>
                    </Row>
                    <br/>

                </Card>



                <Modal isOpen={this.state.selectedUpdate} toggle={this.toggleFormUpdate}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormUpdate}>UPDATE CAREGIVERS: </ModalHeader>
                    <ModalBody>
                        <CareGiverFormUpdate reloadHandler={this.reloadUpdate}/>
                    </ModalBody>

                </Modal>



            </div>
        )

    }
}


export default CaregiverView;
