import {HOST} from '../../../commons/hosts';
import RestApiClient from "../../../commons/api/rest-client";


const endpoint = {
    get_medication: '/medication',
    post_medication: "/medication",
    update_medication:"/medication/update",
    delete_medication:"/medication/delete/"

};

function getMedication(callback) {
    let request = new Request(HOST.backend_api + endpoint.get_medication, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}



function insertMedication(user, callback){
    let request = new Request(HOST.backend_api + endpoint.post_medication , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function updateMedication(user, callback){
    let request = new Request(HOST.backend_api + endpoint.update_medication , {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });


    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deleteMedication(params, callback){
    let request = new Request(HOST.backend_api + endpoint.delete_medication + params, {
        method: 'DELETE'
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    getMedication,
    insertMedication,
    updateMedication,
    deleteMedication
};
