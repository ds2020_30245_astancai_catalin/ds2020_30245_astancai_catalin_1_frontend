import React from 'react';



import {Button, Container, Jumbotron} from 'reactstrap';

import {Switch} from "react-router-dom";
import NavigationBarCaregiver from "../../navigation-bar-caregiver";
import NavigationBarPatient from "../../navigation-bar-patient";


const textStyle = {color: 'black', };

class PatientView extends React.Component {


    render() {

        return (

            <div>

                <NavigationBarPatient/>
                    <Container fluid>
                        <h1 className="display-3" style={textStyle}>Multumim pentru ca ai ales clinica noastra</h1>
                        <h2>Vei primi rezultatele pe email</h2>
                        <p className="lead">
                            <Button color="primary" onClick={() => window.open('http://coned.utcluj.ro/~salomie/DS_Lic/')}>Learn
                                More</Button>
                        </p>
                    </Container>


            </div>
        )
    };
}

export default PatientView
