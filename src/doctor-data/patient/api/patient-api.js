import {HOST} from '../../../commons/hosts';
import RestApiClient from "../../../commons/api/rest-client";


const endpoint = {
    get_patient: '/patient',
    post_patient: "/patient",
    update_patient:"/patient/update",
    delete_patient:"/patient/delete/",
    get_caregiver: '/caregiver/'

};

function getCareGiverID(params, callback){
    let request = new Request(HOST.backend_api + endpoint.get_caregiver + params, {
        method: 'GET'
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}
function getPatient(callback) {
    let request = new Request(HOST.backend_api + endpoint.get_patient, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}



function insertPatient(user, callback){
    let request = new Request(HOST.backend_api + endpoint.post_patient , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },

        body: JSON.stringify(user)
    });
    console.log(JSON.stringify(user))
    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function updatePatient(user, callback){
    let request = new Request(HOST.backend_api + endpoint.update_patient , {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });


    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deletePatient(params, callback){
    let request = new Request(HOST.backend_api + endpoint.delete_patient + params, {
        method: 'DELETE'
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    getPatient,
    insertPatient,
    updatePatient,
    deletePatient,
    getCareGiverID
};
