import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader, NavLink,
    Row
} from 'reactstrap';
import Table from "../../commons/tables/table"
//import MedicationForm from "./medication-form";

import CareGiverFormUpdate from "../caregiver/caregiver-update";
import CareGiverForm from "../caregiver/caregiver-form";
import PatientForm from "./patient-form";
//import MedicationFormUpdate from "./medication-update";

import * as API_USERS from "./api/patient-api"
import PatientFormUpdate from "./patient-update";
import NavigationBar from "../../navigation-bar";
import NavigationBarPatient from "../../navigation-bar-patient";

var columns


const filters = [
    {
        accessor: 'name',
    }
];

class Patient extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.toggleFormUpdate = this.toggleFormUpdate.bind(this);
        this.toggleFormDelete = this.toggleFormDelete.bind(this);

        this.reload = this.reload.bind(this);
        this.reloadUpdate = this.reloadUpdate.bind(this);
        this.reloadDelete = this.reloadDelete.bind(this);
        this.state = {
            selected: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
        columns = [
            {
                Header: 'Username',
                accessor: 'username',
            },


            {
                Header: 'ID',
                accessor: 'id',
            },

            {
                Header: 'Name',
                accessor: 'name',
            },
            {
                Header: 'BirthDate',
                accessor: 'birthDate',
            },

            {
                Header: 'Gender',
                accessor: 'gender',
            },

            {
                Header: 'Medical Record ',
                accessor: 'medicalRecord',
            },



            {
                Header: 'CareGiverID ',
                accessor: 'careGiver.id',
            },

            {
                Header: 'CareGiverID ',
                accessor: 'careGiver.name',
            },
            {
                Header: 'Delete',
                Cell: ({original}) => (
                    <button value={original.id} onClick={this.deletePatient.bind(this, original.id)}>DELETE</button>
                )
            },


        ];
    }
    deletePatient(id) {
        console.log("salut "+id);
        return API_USERS.deletePatient(id, (result, status, error) => {
            if (result !== null && status === 200) {

                console.log("Successfully deleted patient with id: " + result);
                this.reloadDelete();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    componentDidMount() {
        this.fetchMedication();
    }

    fetchMedication() {
        return API_USERS.getPatient((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }




    toggleForm() {
        this.setState({selected: !this.state.selected});
    }
    toggleFormUpdate(){
        this.setState({selectedUpdate: !this.state.selectedUpdate});
    }
    toggleFormDelete() {
        this.setState({selectedDelete: !this.state.selectedDelete});
    }
    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchMedication();
    }
    reloadUpdate(){
        this.setState({
            isLoaded: false
        });
        this.toggleFormUpdate();
        this.fetchMedication();

    }

    reloadDelete() {
        this.setState({
            isLoaded: false
        });
        this.toggleFormDelete();
        this.fetchMedication();
    }

    render() {
        return (
            <div>
                <NavigationBarPatient/>
                <NavLink href="/doctor"><strong>HOME</strong></NavLink>
                <CardHeader>
                    <strong> PATIENTS </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '10', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm}>Add Patient </Button>
                            <p> </p>
                            <Button color="primary" onClick={this.toggleFormUpdate}>UPDATE Patient </Button>
                        </Col>


                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '10', offset: 1}}>
                            {this.state.isLoaded &&
                            <Table
                                data={this.state.tableData}
                                columns={columns}
                                search={filters}
                                pageSize={5}
                            />
                            }

                            {this.state.errorStatus > 0 &&
                            <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />
                            }
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add Patient: </ModalHeader>
                    <ModalBody>
                        <PatientForm reloadHandler={this.reload}/>

                    </ModalBody>

                </Modal>
                <Modal isOpen={this.state.selectedUpdate} toggle={this.toggleFormUpdate}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormUpdate}>UPDATE PATIENT: </ModalHeader>
                    <ModalBody>
                        <PatientFormUpdate reloadHandler={this.reloadUpdate}/>
                    </ModalBody>
                </Modal>

            </div>
        )

    }
}


export default Patient;
