import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'


import Persons from './person-data/person/persons'

import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';
import Caregiver from "./doctor-data/caregiver/caregiver";
import Medication from "./doctor-data/medication/medication";
import CareGiverFormUpdate from "./doctor-data/caregiver/caregiver-update";
import Patient from "./doctor-data/patient/patient";
import Login from "./login/login";
import Home from "./home/home";
import CaregiverView from "./doctor-data/caregiver/caregiverView";
import PatientView from "./doctor-data/patient/patientView";

class App extends React.Component {


    render() {
        return (
            <div className={styles.back}>
            <Router>
                <div>

                    <Switch>

                        <Route
                            exact
                            path='/'
                            render={() => <Login/>}
                        />

                        <Route
                            exact
                            path='/patientView'
                            render={() => <PatientView/>}
                        />

                        <Route
                            exact
                            path='/doctor'
                            render={() => <Home/>}
                        />


                        <Route
                            exact
                            path='/persons'
                            render={() =><Persons/>}
                        />
                        <Route
                            exact
                            path='/patient'
                            render={() =>  <Patient/>}
                        />

                        <Route
                            exact
                            path='/caregiver'
                            render={() => <Caregiver/>}
                        />

                        <Route
                            exact
                            path='/caregiverView'
                            render={() => <CaregiverView/>}
                        />

                        <Route
                            exact
                            path='/caregiver/update'
                            render={() => <CareGiverFormUpdate/>}
                        />
                        <Route
                            exact
                            path='/Medication'
                            render={() => <Medication/>}
                        />
                        {/*Error*/}
                        <Route
                            exact
                            path='/error'
                            render={() => <ErrorPage/>}
                        />
                        <Route render={() =><ErrorPage/>} />
                    </Switch>
                </div>
            </Router>
            </div>
        )
    };
}

export default App
