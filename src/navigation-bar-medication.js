import React from 'react'
import logo from './commons/images/icon.png';

import {
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    Navbar,
    NavbarBrand,
    NavLink,
    UncontrolledDropdown
} from 'reactstrap';

const textStyle = {
    color: 'white',
    textDecoration: 'none'
};

const NavigationBarMedication = () => (
    <div>
        <Navbar color="white" light expand="md">
            <NavbarBrand href="/">
                <img src={logo} width={"50"}
                     height={"35"} />
                <p><strong>logout</strong></p>
            </NavbarBrand>
            <Nav  className="mr-auto" navbar>





                <NavLink href="/medication"><strong>Medication</strong></NavLink>


            </Nav>
        </Navbar>
    </div>
);

export default NavigationBarMedication
